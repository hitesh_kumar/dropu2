package com.dropu2.email.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service("emailService")
public class EmailService 
{

	@Autowired
    private JavaMailSender mailSender;
      
    @Autowired
    private SimpleMailMessage preConfiguredMessage;
  
	public boolean sendMail(EmailDTO emailDTO) {
		
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom(emailDTO.getSenderEmail());
			message.setTo(emailDTO.getReceiverEmail());
			message.setSubject(emailDTO.getEmailSub());
			message.setText(emailDTO.getEmailBody());
			mailSender.send(message);
			return true;
		} catch (MailException e) {
			e.printStackTrace();
			return false;
		}
	}
  
	public boolean verificationLink() {
		return true;
	}
	
    /**
     * This method will send a pre-configured message
     * */
    public void sendPreConfiguredMail(String message) 
    {
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
}