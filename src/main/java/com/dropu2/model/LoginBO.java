package com.dropu2.model;

import com.dropu2.enume.DeviceType;
import com.dropu2.enume.LoginTypeEnum;

/**
 * @author NAROTTAMS
 * @created_date Sep 20, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
public class LoginBO {
	private String emailId;
	private String password;
	private String mobile;
	private LoginTypeEnum loginTypeEnum;
	private String deviceID;
	private String deviceName;
	private String devicePushID;
	private DeviceType deviceType;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public LoginTypeEnum getLoginTypeEnum() {
		return loginTypeEnum;
	}

	public void setLoginTypeEnum(LoginTypeEnum loginTypeEnum) {
		this.loginTypeEnum = loginTypeEnum;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDevicePushID() {
		return devicePushID;
	}

	public void setDevicePushID(String devicePushID) {
		this.devicePushID = devicePushID;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

}
