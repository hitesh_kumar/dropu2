package com.dropu2.model;

/**
 * @author NAROTTAMS
 * @created_date Dec 23, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

public class RideResult {
	Long rtaker_id;
	Integer no_of_traveller;
	String ride_looking_time;
	Long rgiver_id;
	Integer ride_fare;
	String ride_start_time;
	Integer seats_offered;
	Long car_id;
	Double pickupDifference;
	Double dropDiffernce;
	String color;
	String name;
	String number;
	String vehicle_type;
	String first_name;
	String last_name;
	String gender;
	String contact_email;
	String contact_number;
	
	

	public RideResult() {
		super();
	}

	
	
	public RideResult(String first_name, String last_name) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
	}



	public RideResult(Long rtaker_id, Integer no_of_traveller, String ride_looking_time, Long rgiver_id,
			Integer ride_fare, String ride_start_time, Integer seats_offered, Long car_id, Double pickupDifference,
			Double dropDiffernce, String color, String name, String number, String vehicle_type, String first_name,
			String last_name, String gender, String contact_email, String contact_number) {
		super();
		this.rtaker_id = rtaker_id;
		this.no_of_traveller = no_of_traveller;
		this.ride_looking_time = ride_looking_time;
		this.rgiver_id = rgiver_id;
		this.ride_fare = ride_fare;
		this.ride_start_time = ride_start_time;
		this.seats_offered = seats_offered;
		this.car_id = car_id;
		this.pickupDifference = pickupDifference;
		this.dropDiffernce = dropDiffernce;
		this.color = color;
		this.name = name;
		this.number = number;
		this.vehicle_type = vehicle_type;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.contact_email = contact_email;
		this.contact_number = contact_number;
	}

	public Long getRtaker_id() {
		return rtaker_id;
	}

	public void setRtaker_id(Long rtaker_id) {
		this.rtaker_id = rtaker_id;
	}

	public Integer getNo_of_traveller() {
		return no_of_traveller;
	}

	public void setNo_of_traveller(Integer no_of_traveller) {
		this.no_of_traveller = no_of_traveller;
	}

	public String getRide_looking_time() {
		return ride_looking_time;
	}

	public void setRide_looking_time(String ride_looking_time) {
		this.ride_looking_time = ride_looking_time;
	}

	public Long getRgiver_id() {
		return rgiver_id;
	}

	public void setRgiver_id(Long rgiver_id) {
		this.rgiver_id = rgiver_id;
	}

	public Integer getRide_fare() {
		return ride_fare;
	}

	public void setRide_fare(Integer ride_fare) {
		this.ride_fare = ride_fare;
	}

	public String getRide_start_time() {
		return ride_start_time;
	}

	public void setRide_start_time(String ride_start_time) {
		this.ride_start_time = ride_start_time;
	}

	public Integer getSeats_offered() {
		return seats_offered;
	}

	public void setSeats_offered(Integer seats_offered) {
		this.seats_offered = seats_offered;
	}

	public Long getCar_id() {
		return car_id;
	}

	public void setCar_id(Long car_id) {
		this.car_id = car_id;
	}

	public Double getPickupDifference() {
		return pickupDifference;
	}

	public void setPickupDifference(Double pickupDifference) {
		this.pickupDifference = pickupDifference;
	}

	public Double getDropDiffernce() {
		return dropDiffernce;
	}

	public void setDropDiffernce(Double dropDiffernce) {
		this.dropDiffernce = dropDiffernce;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getVehicle_type() {
		return vehicle_type;
	}

	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContact_email() {
		return contact_email;
	}

	public void setContact_email(String contact_email) {
		this.contact_email = contact_email;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

}
