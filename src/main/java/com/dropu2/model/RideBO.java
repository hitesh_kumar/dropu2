package com.dropu2.model;

import java.time.LocalDateTime;
import java.util.Set;

import com.dropu2.enume.StatusCode;
import com.dropu2.ride.entity.Instructions;
import com.dropu2.ride.entity.RideLocation;

public class RideBO {

	private Long rideID;
	private Long carID;
	private Set<Instructions> rideInstructions;
	private LocalDateTime rideStartTime;
	private Integer seatsOffered;
	private Integer rideFare;
	private Boolean recurring;
	private StatusCode statusCode;
	private RideLocation startLocation;
	private RideLocation endLocation;

	public Long getRideID() {
		return rideID;
	}

	public void setRideID(Long rideID) {
		this.rideID = rideID;
	}

	public Long getCarID() {
		return carID;
	}

	public void setCarID(Long carID) {
		this.carID = carID;
	}

	


	public Set<Instructions> getRideInstructions() {
		return rideInstructions;
	}

	public void setRideInstructions(Set<Instructions> rideInstructions) {
		this.rideInstructions = rideInstructions;
	}

	public LocalDateTime getRideStartTime() {
		return rideStartTime;
	}

	public void setRideStartTime(LocalDateTime rideStartTime) {
		this.rideStartTime = rideStartTime;
	}

	public Integer getSeatsOffered() {
		return seatsOffered;
	}

	public void setSeatsOffered(Integer seatsOffered) {
		this.seatsOffered = seatsOffered;
	}

	public Integer getRideFare() {
		return rideFare;
	}

	public void setRideFare(Integer rideFare) {
		this.rideFare = rideFare;
	}

	public Boolean getRecurring() {
		return recurring;
	}

	public void setRecurring(Boolean recurring) {
		this.recurring = recurring;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	public RideLocation getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(RideLocation startLocation) {
		this.startLocation = startLocation;
	}

	public RideLocation getEndLocation() {
		return endLocation;
	}

	public void setEndLocation(RideLocation endLocation) {
		this.endLocation = endLocation;
	}

	

}
