package com.dropu2.ride.payment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.PaymentOption;

/**
 * @author NAROTTAMS
 * @created_date Oct 12, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

@Entity(name = "ride_payment")
public class RidePayment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "payment_option")
	@Enumerated(EnumType.STRING)
	private PaymentOption paymentOption;

}
