package com.dropu2.ride.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.RequestStatus;
import com.dropu2.oauth.entity.CreateUpdateDate;

@Entity(name = "ride_request")
public class RideRequest extends CreateUpdateDate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3276442720683890682L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "ride_id")
	private Long rideID;

	@Column(name = "rider_id")
	private Long riderID;

	@Enumerated(EnumType.STRING)
	@Column(name = "request_status")
	private RequestStatus requestStatus;

	@Column(name = "request_time")
	private Date requestTime;

	@Column(name = "ride_code")
	private Long rideCode;

	@Column(name = "drop_user")
	private Boolean drop;

	@Column(name = "payment_received")
	private Boolean paymentReceived;

	@Column(name = "ride_started")
	private Boolean rideStarted;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Long getRideID() {
		return rideID;
	}

	public void setRideID(Long rideID) {
		this.rideID = rideID;
	}

	public Long getRiderID() {
		return riderID;
	}

	public void setRiderID(Long riderID) {
		this.riderID = riderID;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public Long getRideCode() {
		return rideCode;
	}

	public Boolean isDrop() {
		return drop;
	}

	public void setDrop(Boolean drop) {
		this.drop = drop;
	}

	public Boolean isPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(Boolean paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public Boolean isRideStarted() {
		return rideStarted;
	}

	public void setRideStarted(Boolean rideStarted) {
		this.rideStarted = rideStarted;
	}

	public void setRideCode(Long rideCode) {
		this.rideCode = rideCode;
	}

	
}
