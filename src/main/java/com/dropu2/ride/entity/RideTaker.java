package com.dropu2.ride.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.dropu2.enume.PaymentOption;
import com.dropu2.enume.StatusCode;
import com.dropu2.oauth.entity.CreateUpdateDate;

@Entity(name = "ride_taker")
public class RideTaker extends CreateUpdateDate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5871170942829978067L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "user_id")
	private Long userID;

	@Column(name = "no_of_traveller")
	private Integer noOfTraveller;

	@Column(name = "ride_looking_time")
	private LocalDateTime rideLookingTime;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "start_location_id", referencedColumnName = "id")
	private RideLocation startLocation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "end_location_id", referencedColumnName = "id")
	private RideLocation endLocation;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private StatusCode statusCode;

	@Column(name = "payment_option")
	@Enumerated(EnumType.STRING)
	private PaymentOption paymentOption;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public Integer getNoOfTraveller() {
		return noOfTraveller;
	}

	public void setNoOfTraveller(Integer noOfTraveller) {
		this.noOfTraveller = noOfTraveller;
	}

	public LocalDateTime getRideLookingTime() {
		return rideLookingTime;
	}

	public void setRideLookingTime(LocalDateTime rideLookingTime) {
		this.rideLookingTime = rideLookingTime;
	}

	public RideLocation getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(RideLocation startLocation) {
		this.startLocation = startLocation;
	}

	public RideLocation getEndLocation() {
		return endLocation;
	}

	public void setEndLocation(RideLocation endLocation) {
		this.endLocation = endLocation;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	public PaymentOption getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(PaymentOption paymentOption) {
		this.paymentOption = paymentOption;
	}

}
