package com.dropu2.ride.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.RideLocationType;

@Entity(name = "ride_location")
public class RideLocation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2383677364722887259L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longiture;

	@Column(name = "ride_location_type")
	@Enumerated(EnumType.STRING)
	private RideLocationType rideLocationType;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongiture() {
		return longiture;
	}

	public void setLongiture(Double longiture) {
		this.longiture = longiture;
	}

	public RideLocationType getRideLocationType() {
		return rideLocationType;
	}

	public void setRideLocationType(RideLocationType rideLocationType) {
		this.rideLocationType = rideLocationType;
	}
	
	

}
