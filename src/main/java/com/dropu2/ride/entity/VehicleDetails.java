package com.dropu2.ride.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.StatusCode;
import com.dropu2.enume.VehicleType;
import com.dropu2.oauth.entity.CreateUpdateDate;

@Entity(name = "vehicle_detail")
public class VehicleDetails extends CreateUpdateDate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8371986559174369432L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "name")
	private String name;

	@Column(name = "color")
	private String color;

	@Column(name = "number")
	private String number;

	@Column(name = "model")
	private String model;

	@Column(name = "reg_no")
	private String regNo;

	@Column(name = "vehicle_type")
	@Enumerated(EnumType.STRING)
	private VehicleType vehicleType;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private StatusCode statusCode;

	@Column(name = "owner_id")
	private Long ownerID;

	
	public VehicleDetails() {
		super();
	}

	public VehicleDetails(Long iD) {
		super();
		ID = iD;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public Long getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(Long ownerID) {
		this.ownerID = ownerID;
	}

	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

}
