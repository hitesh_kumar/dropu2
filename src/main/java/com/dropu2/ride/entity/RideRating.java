package com.dropu2.ride.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.dropu2.enume.RideType;
import com.dropu2.oauth.entity.CreateUpdateDate;

/**
 * @author NAROTTAMS
 * @created_date Oct 12, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

@Entity(name = "ride_rating")
public class RideRating extends CreateUpdateDate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2168984677850007399L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "comment")
	private String comment;

	@OneToMany
	private List<Rating> ratings;

	@Enumerated(EnumType.STRING)
	@Column(name = "ride_type")
	private RideType rideType;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

}
