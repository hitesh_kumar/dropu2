package com.dropu2.ride.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.dropu2.enume.StatusCode;
import com.dropu2.oauth.entity.CreateUpdateDate;

@Entity(name = "ride_giver")
public class RideGiver extends CreateUpdateDate {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2383677364722887259L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@OneToOne(optional = true)
	@JoinColumn(name = "car_id", referencedColumnName = "ID")
	private VehicleDetails carDetails;

	@ManyToMany
	private Set<Instructions> instructions;

	@Column(name = "ride_start_time")
	private LocalDateTime rideStartTime;

	@Column(name = "seats_offered")
	private Integer seatsOffered;

	@Column(name = "ride_fare")
	private Integer rideFare;

	@Column(name = "recurring")
	private Boolean recurring;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private StatusCode statusCode;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "start_location_id", referencedColumnName = "id")
	private RideLocation startLocation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "end_location_id", referencedColumnName = "id")
	private RideLocation endLocation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pickup_location_id", referencedColumnName = "id")
	private RideLocation pickupLocation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "drop_location_id", referencedColumnName = "id")
	private RideLocation dropLocation;

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public VehicleDetails getCarDetails() {
		return carDetails;
	}

	public void setCarDetails(VehicleDetails carDetails) {
		this.carDetails = carDetails;
	}

	

	public Set<Instructions> getInstructions() {
		return instructions;
	}

	public void setInstructions(Set<Instructions> instructions) {
		this.instructions = instructions;
	}

	public LocalDateTime getRideStartTime() {
		return rideStartTime;
	}

	public void setRideStartTime(LocalDateTime rideStartTime) {
		this.rideStartTime = rideStartTime;
	}

	public Integer getSeatsOffered() {
		return seatsOffered;
	}

	public void setSeatsOffered(Integer seatsOffered) {
		this.seatsOffered = seatsOffered;
	}

	public Integer getRideFare() {
		return rideFare;
	}

	public void setRideFare(Integer rideFare) {
		this.rideFare = rideFare;
	}

	public Boolean getRecurring() {
		return recurring;
	}

	public void setRecurring(Boolean recurring) {
		this.recurring = recurring;
	}

	public RideLocation getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(RideLocation startLocation) {
		this.startLocation = startLocation;
	}

	public RideLocation getEndLocation() {
		return endLocation;
	}

	public void setEndLocation(RideLocation endLocation) {
		this.endLocation = endLocation;
	}

	public RideLocation getPickupLocation() {
		return pickupLocation;
	}

	public void setPickupLocation(RideLocation pickupLocation) {
		this.pickupLocation = pickupLocation;
	}

	public RideLocation getDropLocation() {
		return dropLocation;
	}

	public void setDropLocation(RideLocation dropLocation) {
		this.dropLocation = dropLocation;
	}

}
