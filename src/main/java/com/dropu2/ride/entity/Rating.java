package com.dropu2.ride.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author NAROTTAMS
 * @created_date Oct 16, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

@Entity(name = "rating")
public class Rating {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@OneToOne(optional = true)
	@JoinColumn(name = "rating_category_id", referencedColumnName = "id")
	private RatingCategory ratingCategory;

	@Column(name = "rate")
	private Integer rate;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public RatingCategory getRatingCategory() {
		return ratingCategory;
	}

	public void setRatingCategory(RatingCategory ratingCategory) {
		this.ratingCategory = ratingCategory;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}
	
	
}
