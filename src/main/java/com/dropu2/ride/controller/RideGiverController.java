package com.dropu2.ride.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.RideBO;
import com.dropu2.ride.entity.RideGiver;
import com.dropu2.ride.service.RideGiverService;

/**
 * @author NAROTTAMS
 * @created_date Oct 7, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@RestController
@RequestMapping("/ridegiver")
public class RideGiverController {

	@javax.annotation.Resource
	private RideGiverService rideGiverService;

	@RequestMapping(value = "/cteate", method = RequestMethod.POST)
	public ResponseEntity<RideGiver> createRide(@RequestBody RideBO ride) {
		RideGiver rideDB = rideGiverService.createRide(ride);
		if (rideDB != null)
			return new ResponseEntity<>(rideDB, HttpStatus.CREATED);
		else
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/fetch/byowner/{owner_id}", method = RequestMethod.GET)
	public ResponseEntity<List<RideGiver>> fetchByOwner(@PathVariable("owner_id") Long owner_id) {
		return ResponseEntity.ok().body(rideGiverService.fetchRideByOwner(owner_id));
	}

	@RequestMapping(value = "/fetch/byvehicle/{vehicle_id}", method = RequestMethod.GET)
	public ResponseEntity<List<RideGiver>> fetchByVehicle(@PathVariable("vehicle_id") Long vehicle_id) {
		return ResponseEntity.ok().body(rideGiverService.fetchRideByVehicle(vehicle_id));
	}

	@RequestMapping(value = "/fetch/byownerandstatus/{owner_id}/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<RideGiver>> fetchRideByOwnerAndStatus(@PathVariable("owner_id") Long owner_id,
			@PathVariable("status") StatusCode status) {
		return ResponseEntity.ok().body(rideGiverService.fetchRideByOwnerAndStatus(owner_id, status));
	}

	@RequestMapping(value = "/update/ridestatus/{id}/{status}", method = RequestMethod.GET)
	public ResponseEntity<Integer> updateridestatus(@PathVariable("id") Long id,
			@PathVariable("status") StatusCode status) {
		return ResponseEntity.ok().body(rideGiverService.updateRideStatus(status, id));
	}

}
