package com.dropu2.ride.controller;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.enume.RequestStatus;
import com.dropu2.ride.entity.RideRequest;
import com.dropu2.ride.service.RideRequestService;

/**
 * @author NAROTTAMS
 * @created_date Oct 11, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

@RestController
@RequestMapping("/riderequest")
public class RideRequestController {

	@javax.annotation.Resource
	private RideRequestService requestService;

	@RequestMapping(value = "/sendrequest", method = RequestMethod.POST)
	public ResponseEntity<RideRequest> sendRequest(@RequestBody RideRequest rideRequest) {
		RideRequest rDB = requestService.fetchRideRequestByRideAndRiderID(rideRequest.getRideID(),
				rideRequest.getRiderID());
		if (rDB == null) {
			rideRequest.setCreatedTime(new Date());
			return ResponseEntity.ok(this.requestService.sendRequest(rideRequest));
		} else {
			return new ResponseEntity<RideRequest>(rDB, HttpStatus.ALREADY_REPORTED);
		}
	}

	@RequestMapping(value = "/update/ridestatus/{id}/{requestStatus}", method = RequestMethod.GET)
	public ResponseEntity<Integer> updateridestatus(@PathVariable("id") Long id,
			@PathVariable("requestStatus") RequestStatus requestStatus) {
		return ResponseEntity.ok().body(requestService.updateStatus(requestStatus, id));
	}

	@RequestMapping(value = "/update/ridepayment/{id}/{payment}", method = RequestMethod.GET)
	public ResponseEntity<Integer> updateridepayment(@PathVariable("id") Long id,
			@PathVariable("payment") Boolean payment) {
		return ResponseEntity.ok().body(requestService.updateRidePayment(payment, id));
	}

	@RequestMapping(value = "/update/ridedrop/{id}/{drop}", method = RequestMethod.GET)
	public ResponseEntity<Integer> updateridedrop(@PathVariable("id") Long id, @PathVariable("drop") Boolean drop) {
		return ResponseEntity.ok().body(requestService.updateRideDrop(drop, id));
	}

	@RequestMapping(value = "/fetch/ridebyrideandstatus/{rideid}/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<RideRequest>> fetchRideRequestByRide(@PathVariable("rideid") Long rideid,
			@PathVariable("status") RequestStatus requestStatus) {
		return ResponseEntity.ok().body(requestService.fetchRideRequestByRideAndStatus(rideid, requestStatus));
	}

	@RequestMapping(value = "/fetch/ridebyriderandstatus/{riderid}/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<RideRequest>> fetchRiderRequestByRide(@PathVariable("riderid") Long riderid,
			@PathVariable("status") RequestStatus requestStatus) {
		return ResponseEntity.ok().body(requestService.fetchRideRequestByRiderAndStatus(riderid, requestStatus));
	}

	@RequestMapping(value = "/fetch/ridebyrideandrider/{riderid}/{rideid}", method = RequestMethod.GET)
	public ResponseEntity<RideRequest> fetchRiderRequestByRideAndRider(@PathVariable("riderid") Long riderid,
			@PathVariable("rideid") Long rideid) {
		return ResponseEntity.ok().body(requestService.fetchRideRequestByRideAndRiderID(rideid, riderid));
	}
	
	

}
