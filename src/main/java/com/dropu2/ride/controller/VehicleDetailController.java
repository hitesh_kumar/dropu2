package com.dropu2.ride.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.enume.StatusCode;
import com.dropu2.ride.entity.VehicleDetails;
import com.dropu2.ride.service.VehicleDetailService;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@RestController
@RequestMapping("/vehicledetails")
public class VehicleDetailController {

	@javax.annotation.Resource
	private VehicleDetailService carDetailService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<List<VehicleDetails>> fetchCarList() {
		return  ResponseEntity.ok(carDetailService.fetchAllVehicleDetails());
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<VehicleDetails> saveCarDetails(@RequestBody VehicleDetails carDetails) {
		return new ResponseEntity<>(carDetailService.saveVihicleDetail(carDetails), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/findbynumber/{platNumber}", method = RequestMethod.GET)
	public ResponseEntity<VehicleDetails> findByPlatNumber(@PathVariable("platNumber") String platNumber) {
		return ResponseEntity.ok(carDetailService.findVehicleByNumber(platNumber));
	}

	@RequestMapping(value = "/findbyowner/{owner_id}", method = RequestMethod.GET)
	public ResponseEntity<List<VehicleDetails>> findByUser(@PathVariable("owner_id") Long owner_id) {
		return ResponseEntity.ok(carDetailService.findVehicleByOwner(owner_id));
	}

	@RequestMapping(value = "/update/{ID}/{status_code}", method = RequestMethod.GET)
	public ResponseEntity<?> updateVehicleStatus(@PathVariable("ID") Long ID, @PathVariable("status_code") StatusCode status_code) {
		return ResponseEntity.ok(carDetailService.updateVehicle(status_code, ID));
	}
}
