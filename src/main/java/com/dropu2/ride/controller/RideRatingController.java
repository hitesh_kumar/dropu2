package com.dropu2.ride.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.ride.entity.RideRating;
import com.dropu2.ride.service.RideRatingService;

/**
 * @author NAROTTAMS
 * @created_date Oct 16, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

@RestController
@RequestMapping("/rating")
public class RideRatingController {

	@javax.annotation.Resource
	private RideRatingService ratingService;

	@RequestMapping(name = "/rate")
	public ResponseEntity<RideRating> rating(@RequestBody RideRating rideRating) {
		return ResponseEntity.ok(ratingService.rating(rideRating));
	}
}
