package com.dropu2.ride.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.RideResult;
import com.dropu2.ride.entity.RideTaker;
import com.dropu2.ride.service.RideTakerService;

/**
 * @author NAROTTAMS
 * @created_date Oct 7, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@RestController
@RequestMapping("/ridetaker")
public class RideTakerController {

	@javax.annotation.Resource
	private RideTakerService riderService;

	@RequestMapping(value = "/cteate", method = RequestMethod.POST)
	public ResponseEntity<RideTaker> createRide(@RequestBody RideTaker ride) {
		RideTaker rideDB = riderService.createRide(ride);
		if (rideDB != null)
			return new ResponseEntity<>(rideDB, HttpStatus.CREATED);
		else
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/fetch/login/{login_id}", method = RequestMethod.GET)
	public ResponseEntity<List<RideTaker>> fetchByLogin(@PathVariable("login_id") Long login_id) {
		return ResponseEntity.ok().body(riderService.fetchRiderLogin(login_id));
	}

	@RequestMapping(value = "/fetch/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<RideTaker>> fetchByID(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(riderService.fetchByID(id));
	}

	@RequestMapping(value = "/fetch/loginandstatus/{login_id}/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<RideTaker>> fetchRideByOwnerAndStatus(@PathVariable("login_id") Long login_id,
			@PathVariable("status") StatusCode status) {
		return ResponseEntity.ok().body(riderService.fetchRideByLoginAndStatus(login_id, status));
	}

	@RequestMapping(value = "/update/ridestatus/{id}/{status}", method = RequestMethod.GET)
	public ResponseEntity<Integer> updateridestatus(@PathVariable("id") Long id,
			@PathVariable("status") StatusCode status) {
		return ResponseEntity.ok().body(riderService.updateRideStatus(status, id));
	}

	@RequestMapping(value = "/find/{ridetakerid}/ridegiver", method = RequestMethod.GET)
	public ResponseEntity<List<RideResult>> findMatchRideGiver(@PathVariable("ridetakerid") Long ridetakerid) {
		return ResponseEntity.ok().body(riderService.findMatchRideGiver(ridetakerid));
	}
}
