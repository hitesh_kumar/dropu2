package com.dropu2.ride.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.dropu2.enume.StatusCode;
import com.dropu2.ride.entity.RideGiver;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface RideGiverRepository extends JpaRepository<RideGiver, Long> {
	@Query("SELECT e from #{#entityName} e where carDetails.ownerID=:ownerID")
	List<RideGiver> findByOwnerID(Long ownerID);

	@Query("SELECT e from #{#entityName} e where carDetails.ID=:vehicleID")
	List<RideGiver> findByVehicleID(Long vehicleID);

	@Query("SELECT e from #{#entityName} e where carDetails.ownerID=:ownerID and statusCode=:statusCode")
	List<RideGiver> findByOwnerAndStatus(Long ownerID, StatusCode statusCode);

	@Query("UPDATE  #{#entityName} e set e.statusCode=:statusCode where ID=:ID")
	Integer updateStatus(StatusCode statusCode, Long ID);

}
