package com.dropu2.ride.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dropu2.enume.StatusCode;
import com.dropu2.ride.entity.RideTaker;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface RiderTakerRepository extends JpaRepository<RideTaker, Long> {
	List<RideTaker> findByUserID(@Param("userID") Long loginID);

	List<RideTaker> findByUserIDAndStatusCode(@Param("userID") Long loginID, @Param("statusCode") StatusCode statusCode);

	@Query("UPDATE  #{#entityName} e set e.statusCode=:statusCode where ID=:ID")
	Integer updateStatus(StatusCode statusCode, Long ID);
	
	@Query(value = "select rt.id rtaker_id, rt.no_of_traveller, rt.ride_looking_time," + 
			"rg.id rgiver_id, rg.ride_fare, rg.ride_start_time, rg.seats_offered,  rg.car_id," + 
			"distanceCalculate(rt.start_location_id,rg.start_location_id) as pickupDifference, "
			+ "distanceCalculate(rt.end_location_id, rg.end_location_id) as dropDiffernce," + 
			"IFNULL(vd.color,'NA'), IFNULL(vd.name,'NA'), vd.number, vd.vehicle_type," + 
			"IFNULL(ud.first_name,'NA'), IFNULL(ud.last_name,'NA'), IFNULL(ud.gender,'NA'), "
			+ "IFNULL(ud.contact_email,'NA'), IFNULL(ud.contact_number,'NA') " + 
			"from ride_taker rt join ride_giver rg on (DATE_FORMAT(rt.ride_looking_time,'%d/%m/%Y')=DATE_FORMAT(rg.ride_start_time,'%d/%m/%Y'))" + 
			"join vehicle_detail vd on (vd.id=rg.car_id)" + 
			"join user_detail ud on (ud.login_id=vd.owner_id)" + 
			"where rg.status='ENABLE' and rt.id=?1 order by pickupDifference , dropDiffernce limit 10",nativeQuery = true)
	List<Object[]> findGivenRider(Long rideTakerId);
	
	

}
