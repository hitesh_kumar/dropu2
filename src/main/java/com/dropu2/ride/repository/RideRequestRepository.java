package com.dropu2.ride.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dropu2.enume.RequestStatus;
import com.dropu2.ride.entity.RideRequest;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

public interface RideRequestRepository extends JpaRepository<RideRequest, Long> {

	@Query("UPDATE  #{#entityName} e set e.requestStatus=:requestStatus, e.modifyTime=now() where ID=:ID")
	Integer updateStatus(RequestStatus requestStatus, Long ID);

	@Query("UPDATE  #{#entityName} e set e.paymentReceived=:paymentReceived, e.modifyTime=now() where ID=:ID")
	Integer updateRidePayment(Boolean paymentReceived, Long ID);

	@Query("UPDATE  #{#entityName} e set e.drop=:drop, e.modifyTime=now() where ID=:ID")
	Integer updateRideDrop(Boolean drop, Long ID);

	List<RideRequest> findByRideIDAndRequestStatus(@Param("rideID") Long rideID,
			@Param("requestStatus") RequestStatus requestStatus);

	RideRequest findByRideIDAndRiderID(@Param("rideID") Long rideID, @Param("riderID") Long riderID);

	List<RideRequest> findByRiderIDAndRequestStatus(@Param("riderID") Long riderID,
			@Param("requestStatus") RequestStatus requestStatus);

}
