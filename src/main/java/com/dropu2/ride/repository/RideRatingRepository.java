package com.dropu2.ride.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dropu2.ride.entity.RideRating;

/**
 * @author NAROTTAMS
 * @created_date Oct 16, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

public interface RideRatingRepository extends JpaRepository<RideRating, Long> {

}
