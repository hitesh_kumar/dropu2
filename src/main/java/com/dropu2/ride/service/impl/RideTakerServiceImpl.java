package com.dropu2.ride.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.RideResult;
import com.dropu2.ride.entity.RideTaker;
import com.dropu2.ride.repository.RiderTakerRepository;
import com.dropu2.ride.service.RideTakerService;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class RideTakerServiceImpl implements RideTakerService {

	@javax.annotation.Resource
	private RiderTakerRepository riderRideRepository;

	@Override
	public List<RideTaker> fetchRiderLogin(Long loginID) {
		return this.riderRideRepository.findByUserID(loginID);
	}

	@Override
	public List<RideTaker> fetchRideByLoginAndStatus(Long loginID, StatusCode statusCode) {
		return this.riderRideRepository.findByUserIDAndStatusCode(loginID, statusCode);
	}

	@Override
	public RideTaker createRide(RideTaker ride) {
		if (ride.getID() == null || ride.getID() == 0) {
			ride.setCreatedTime(new Date());
		} else {
			ride.setModifyTime(new Date());
		}
		return this.riderRideRepository.save(ride);
	}

	@Override
	public Integer updateRideStatus(StatusCode statusCode, Long ID) {
		return this.riderRideRepository.updateStatus(statusCode, ID);
	}

	@Override
	public Optional<RideTaker> fetchByID(Long ID) {
		return riderRideRepository.findById(ID);
	}

	@Override
	public List<RideResult> findMatchRideGiver(Long rideTakerId) {
		List<Object[]> obj = this.riderRideRepository.findGivenRider(rideTakerId);
		List<RideResult> rR = new ArrayList<>();
		if (obj != null && obj.size() > 0) {
			for (Object[] o : obj) {
				int i = 0;
				RideResult r = new RideResult(Long.valueOf(o[i++].toString()), Integer.valueOf(o[i++].toString()),
						o[i++].toString(), Long.valueOf(o[i++].toString()), Integer.valueOf(o[i++].toString()),
						o[i++].toString(), Integer.valueOf(o[i++].toString()), Long.valueOf(o[i++].toString()),
						Double.valueOf(o[i++].toString()), Double.valueOf(o[i++].toString()), o[i++].toString(),
						o[i++].toString(), o[i++].toString(), o[i++].toString(), o[i++].toString(), o[i++].toString(),
						o[i++].toString(), o[i++].toString(), o[i++].toString());
				rR.add(r);
			}

		}
		return rR;
	}

}
