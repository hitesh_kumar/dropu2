package com.dropu2.ride.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.RideBO;
import com.dropu2.ride.entity.RideGiver;
import com.dropu2.ride.entity.VehicleDetails;
import com.dropu2.ride.repository.RideGiverRepository;
import com.dropu2.ride.service.RideGiverService;

/**
 * @author NAROTTAMS
 * @created_date Oct 8, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class RideGiverServiceImpl implements RideGiverService {

	@javax.annotation.Resource
	private RideGiverRepository rideRepository;

	@Override
	public List<RideGiver> fetchRideByOwner(Long ownerID) {
		return rideRepository.findByOwnerID(ownerID);
	}

	@Override
	public List<RideGiver> fetchRideByOwnerAndStatus(Long ownerID, StatusCode statusCode) {
		return rideRepository.findByOwnerAndStatus(ownerID, statusCode);
	}

	@Override
	public RideGiver createRide(RideBO ride) {
		RideGiver entity = new RideGiver();
		if (ride.getRideID() == null || ride.getRideID() == 0) {
			entity.setCreatedTime(new Date());
		} else {
			entity.setModifyTime(new Date());
		}
		entity.setCarDetails(new VehicleDetails(ride.getCarID()));
		entity.setRecurring(ride.getRecurring());
		entity.setRideFare(ride.getRideFare());
		entity.setInstructions(ride.getRideInstructions());
		entity.setStartLocation(ride.getStartLocation());
		entity.setEndLocation(ride.getEndLocation());
		entity.setStatusCode(ride.getStatusCode());
		entity.setRideStartTime(ride.getRideStartTime());
		entity.setSeatsOffered(ride.getSeatsOffered());
		return rideRepository.save(entity);
	}

	@Override
	public Integer updateRideStatus(StatusCode statusCode, Long ID) {
		return rideRepository.updateStatus(statusCode, ID);
	}

	@Override
	public List<RideGiver> fetchRideByVehicle(Long vehicleID) {
		return rideRepository.findByVehicleID(vehicleID);
	}

}
