package com.dropu2.ride.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dropu2.enume.StatusCode;
import com.dropu2.oauth.repository.VehicleDetailRepository;
import com.dropu2.ride.entity.VehicleDetails;
import com.dropu2.ride.service.VehicleDetailService;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class VehicleDetailServiceImpl implements VehicleDetailService {

	@Resource
	private VehicleDetailRepository vDetailRepository;

	@Override
	public List<VehicleDetails> fetchAllVehicleDetails() {
		return vDetailRepository.findAll();
	}

	@Override
	public VehicleDetails saveVihicleDetail(VehicleDetails vDetails) {
		if (vDetails.getID() == null || vDetails.getID() == 0) {
			vDetails.setCreatedTime(new Date());
		} else {
			vDetails.setModifyTime(new Date());
		}
		return vDetailRepository.save(vDetails);
	}

	@Override
	public VehicleDetails findVehicleByNumber(String number) {
		return vDetailRepository.findByNumber(number);
	}

	@Override
	public List<VehicleDetails> findVehicleByOwner(Long owner_id) {
		return vDetailRepository.findByOwnerID(owner_id);
	}

	@Override
	public Integer updateVehicle(StatusCode statusCode, Long ID) {
		return vDetailRepository.updateVehicleDetailsSetStatusCodeForId(statusCode, ID);
	}

}
