package com.dropu2.ride.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dropu2.enume.RequestStatus;
import com.dropu2.ride.entity.RideRequest;
import com.dropu2.ride.repository.RideRequestRepository;
import com.dropu2.ride.service.RideRequestService;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class RideRequestServiceImpl implements RideRequestService {

	@javax.annotation.Resource
	private RideRequestRepository rideRequestRepository;

	@Override
	public RideRequest sendRequest(RideRequest rideRequest) {
		return this.rideRequestRepository.save(rideRequest);
	}

	@Override
	public Integer updateStatus(RequestStatus requestStatus, Long ID) {
		return this.rideRequestRepository.updateStatus(requestStatus, ID);
	}

	@Override
	public List<RideRequest> fetchRideRequestByRideAndStatus(Long rideID, RequestStatus requestStatus) {
		return this.rideRequestRepository.findByRideIDAndRequestStatus(rideID, requestStatus);
	}

	@Override
	public List<RideRequest> fetchRideRequestByRiderAndStatus(Long riderID, RequestStatus requestStatus) {
		return this.rideRequestRepository.findByRiderIDAndRequestStatus(riderID, requestStatus);
	}

	@Override
	public RideRequest fetchRideRequestByRideAndRiderID(Long rideID, Long riderID) {
		return this.rideRequestRepository.findByRideIDAndRiderID(rideID, riderID);
	}

	@Override
	public Integer updateRidePayment(Boolean payment, Long ID) {
		return null;
	}

	@Override
	public Integer updateRideDrop(Boolean drop, Long ID) {
		return null;
	}

}
