package com.dropu2.ride.service.impl;

import org.springframework.stereotype.Service;

import com.dropu2.ride.entity.RideRating;
import com.dropu2.ride.repository.RideRatingRepository;
import com.dropu2.ride.service.RideRatingService;

/**
 * @author NAROTTAMS
 * @created_date Oct 16, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class RideRatingServiceImpl implements RideRatingService {

	@javax.annotation.Resource
	private RideRatingRepository ratingRepository;

	@Override
	public RideRating rating(RideRating rideRating) {
		return ratingRepository.save(rideRating);
	}

}
