package com.dropu2.ride.service;

import java.util.List;

import com.dropu2.enume.StatusCode;
import com.dropu2.ride.entity.VehicleDetails;



/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
public interface VehicleDetailService {

	List<VehicleDetails> fetchAllVehicleDetails();
	
	List<VehicleDetails> findVehicleByOwner(Long owner_id);
	
	VehicleDetails saveVihicleDetail(VehicleDetails vehicleDetails);
	
	VehicleDetails findVehicleByNumber(String number);
	
	Integer updateVehicle(StatusCode statusCode, Long ID);
}
