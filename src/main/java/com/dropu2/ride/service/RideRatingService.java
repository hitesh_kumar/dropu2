package com.dropu2.ride.service;

import com.dropu2.ride.entity.RideRating;

/**
 * @author NAROTTAMS
 * @created_date Oct 16, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

public interface RideRatingService {
	RideRating rating(RideRating rideRating);
}
