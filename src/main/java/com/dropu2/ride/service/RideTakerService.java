package com.dropu2.ride.service;

import java.util.List;
import java.util.Optional;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.RideResult;
import com.dropu2.ride.entity.RideTaker;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface RideTakerService {
	List<RideTaker> fetchRiderLogin(Long loginID);

	List<RideTaker> fetchRideByLoginAndStatus(Long loginID, StatusCode statusCode);

	RideTaker createRide(RideTaker ride);

	Optional<RideTaker> fetchByID(Long ID);

	Integer updateRideStatus(StatusCode statusCode, Long ID);
	
	List<RideResult> findMatchRideGiver(Long ridetakerid);

}
