package com.dropu2.ride.service;

import java.util.List;

import com.dropu2.enume.RequestStatus;
import com.dropu2.ride.entity.RideRequest;

/**
 * @author NAROTTAMS
 * @created_date Oct 10, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */

public interface RideRequestService {

	RideRequest sendRequest(RideRequest rideRequest);

	Integer updateStatus(RequestStatus requestStatus, Long ID);

	List<RideRequest> fetchRideRequestByRideAndStatus(Long rideID, RequestStatus requestStatus);
	
	List<RideRequest> fetchRideRequestByRiderAndStatus(Long riderID, RequestStatus requestStatus);

	RideRequest fetchRideRequestByRideAndRiderID(Long rideID, Long riderID);
	

	Integer updateRidePayment(Boolean payment, Long ID);

	Integer updateRideDrop(Boolean drop, Long ID);
	
}
