package com.dropu2.ride.service;

import java.util.List;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.RideBO;
import com.dropu2.ride.entity.RideGiver;

/**
 * @author NAROTTAMS
 * @created_date Oct 8, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface RideGiverService {

	List<RideGiver> fetchRideByOwner(Long ownerID);

	List<RideGiver> fetchRideByVehicle(Long vehicleID);

	List<RideGiver> fetchRideByOwnerAndStatus(Long ownerID, StatusCode statusCode);

	RideGiver createRide(RideBO ride);

	Integer updateRideStatus(StatusCode statusCode, Long ID);

}
