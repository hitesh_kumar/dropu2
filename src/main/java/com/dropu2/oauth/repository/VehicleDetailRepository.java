package com.dropu2.oauth.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dropu2.enume.StatusCode;
import com.dropu2.ride.entity.VehicleDetails;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Repository("carDetailRepository")
public interface VehicleDetailRepository extends JpaRepository<VehicleDetails, Long> {
	VehicleDetails findByNumber(@Param("numbe") String numbe);

	List<VehicleDetails> findByOwnerID(@Param("ownerID") Long ownerID);

	@Transactional
	@Modifying
	@Query("update #{#entityName} e set e.statusCode=:statusCode where e.ID=:ID")
	int updateVehicleDetailsSetStatusCodeForId(@Param("statusCode") StatusCode statusCode, @Param("ID") Long ID);
}
