package com.dropu2.oauth.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.dropu2.oauth.entity.UserDetail;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface UserDetailRepository extends PagingAndSortingRepository<UserDetail, Long> {
	UserDetail findByLoginId(@Param("loginID") Long loginId);
}
