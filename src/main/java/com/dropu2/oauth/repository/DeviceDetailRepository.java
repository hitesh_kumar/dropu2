package com.dropu2.oauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.dropu2.enume.StatusCode;
import com.dropu2.oauth.entity.DeviceDetails;

/**
 * @author NAROTTAMS
 * @created_date Sep 26, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface DeviceDetailRepository extends JpaRepository<DeviceDetails, Long> {

	DeviceDetails findByLoginIDAndStatusCode(@Param("loginID") Long loginID, @Param("statusCode") StatusCode statusCode);

}
