package com.dropu2.oauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.dropu2.oauth.entity.ImageData;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
public interface ImageDataRepository extends JpaRepository<ImageData, Long> {
	ImageData findByRefID(@Param("refID") Long ID);
}
