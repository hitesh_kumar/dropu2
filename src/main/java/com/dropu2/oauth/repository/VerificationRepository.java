package com.dropu2.oauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.dropu2.enume.VerificationType;
import com.dropu2.oauth.entity.Verification;

/**
 * @author NAROTTAMS
 * @created_date Sep 20, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
public interface VerificationRepository extends JpaRepository<Verification, Long> {
	Verification findByVerificationTypeAndRefID(@Param("verificationType") VerificationType verificationType,
			@Param("refID") Long refID);
	Verification findByVerificationTypeAndRefIDAndVerificationCode(@Param("verificationType") VerificationType verificationType,
			@Param("refID") Long refID,@Param("verificationCode") String verificationCode);
	

}
