package com.dropu2.oauth.service;

import org.springframework.web.multipart.MultipartFile;

import com.dropu2.oauth.entity.ImageData;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
public interface ImageDataService {
ImageData  findByRefID(Long ID);

ImageData saveImage(MultipartFile file, Long refID);
}
