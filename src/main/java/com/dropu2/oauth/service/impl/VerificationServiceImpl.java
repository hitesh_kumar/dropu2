package com.dropu2.oauth.service.impl;

import java.util.Date;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import com.dropu2.email.util.EmailDTO;
import com.dropu2.email.util.EmailService;
import com.dropu2.enume.StatusCode;
import com.dropu2.enume.VerificationType;
import com.dropu2.oauth.entity.Verification;
import com.dropu2.oauth.repository.VerificationRepository;
import com.dropu2.oauth.service.VerificationService;
/**
 * @author NAROTTAMS
 * @created_date Sep 20, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
@Service
public class VerificationServiceImpl implements VerificationService {

	@javax.annotation.Resource
	private VerificationRepository vR;

	@javax.annotation.Resource
	private EmailService eService;
	
	@Override
	public Verification generateVerificationCode(VerificationType verification_type, String email_id, Long id) {
		Verification vFD = vR.findByVerificationTypeAndRefID(verification_type, id);
		if(vFD!=null)
		{
			vFD.setVerificationCode(""+new Random().nextInt(999999));
			vFD.setVerificationLink(DigestUtils.md5Hex(email_id+id+verification_type));
			vFD.setvStatusCode(StatusCode.VERIFICATION_PENDING);
		}
		else {
			vFD=new Verification();
			vFD.setCreatedTime(new Date());
			vFD.setExpireInHours(24);
			vFD.setRefID(id);
			vFD.setVerificationLink(DigestUtils.md5Hex(email_id+id+verification_type));
			vFD.setVerificationCode(""+new Random().nextInt(999999));
			vFD.setvStatusCode(StatusCode.VERIFICATION_PENDING);
			vFD.setVerificationType(verification_type);
			
		}
		vFD=vR.save(vFD);
		
		return vFD;
	}

	@Override
	public Verification validateVerificationCode(VerificationType verification_type,  Long id,
			String verifyCode) {
		Verification vFD = vR.findByVerificationTypeAndRefIDAndVerificationCode(verification_type, id, verifyCode);
		return vFD;
	}

	@Override
	public boolean sentEmail(String email_id, Verification verification) {
		EmailDTO emailDTO=new EmailDTO();
		emailDTO.setEmailSub("Email Verification");
		emailDTO.setReceiverEmail(email_id);
		emailDTO.setEmailBody("Hi,\n Here is 6 digit email verification code is: "+verification.getVerificationCode()+"\n Thanks,\nTeam");
		emailDTO.setSenderEmail("droup@gamil.com");
		emailDTO.setSenderName("Droupu Team");
		return eService.sendMail(emailDTO);
	}

	@Override
	public Verification save(Verification verification) {
		
		return vR.save(verification);
	}

}
