package com.dropu2.oauth.service.impl;

import java.io.IOException;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.dropu2.oauth.entity.ImageData;
import com.dropu2.oauth.repository.ImageDataRepository;
import com.dropu2.oauth.service.ImageDataService;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
@Service
public class ImageDataServiceImpl implements ImageDataService {

	@javax.annotation.Resource
	private ImageDataRepository iRepository;

	@Override
	public ImageData findByRefID(Long ID) {

		return iRepository.findByRefID(ID);
	}

	@Override
	public ImageData saveImage(MultipartFile file, Long refID) {
		ImageData imageData = iRepository.findByRefID(refID);
		if (imageData == null) {
			imageData = new ImageData();
			imageData.setCreatedTime(new Date());
			imageData.setRefID(refID);
		} 
		imageData.setContentType(file.getContentType());
		try {
			imageData.setImageByte(file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		imageData.setName(file.getOriginalFilename());
		imageData.setModifyTime(new Date());
		return iRepository.save(imageData);
	}

}
