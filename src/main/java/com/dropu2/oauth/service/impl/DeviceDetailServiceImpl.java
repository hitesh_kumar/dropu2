package com.dropu2.oauth.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.LoginBO;
import com.dropu2.oauth.entity.DeviceDetails;
import com.dropu2.oauth.repository.DeviceDetailRepository;
import com.dropu2.oauth.service.DeviceDetailService;

/**
 * @author NAROTTAMS
 * @created_date Sep 26, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class DeviceDetailServiceImpl implements DeviceDetailService {

	@javax.annotation.Resource
	private DeviceDetailRepository deviceDetailRepository;

	@Override
	public DeviceDetails findDeviceByLoginIDStatusCode(Long login_id, StatusCode statusCode) {
		return this.deviceDetailRepository.findByLoginIDAndStatusCode(login_id, statusCode);
	}

	@Override
	public DeviceDetails saveDevice(LoginBO loginBO, Long login_id) {
		DeviceDetails deviceDetails = deviceDetailRepository.findByLoginIDAndStatusCode(login_id, StatusCode.ENABLE);
		if (deviceDetails!=null && !loginBO.getDeviceID().equalsIgnoreCase(deviceDetails.getDeviceID())) {
			deviceDetails.setStatusCode(StatusCode.DISABLE);
			deviceDetails.setModifyTime(new Date());
			this.deviceDetailRepository.save(deviceDetails);
			deviceDetails=new DeviceDetails();
			deviceDetails.setLoginID(login_id);
			deviceDetails.setCreatedTime(new Date());
			deviceDetails.setDeviceID(loginBO.getDeviceID());
			deviceDetails.setDeviceName(loginBO.getDeviceName());
			deviceDetails.setDevicePushID(loginBO.getDevicePushID());
			deviceDetails.setDeviceType(loginBO.getDeviceType());
			deviceDetails.setStatusCode(StatusCode.ENABLE);
		}else if(deviceDetails==null)
		{
			deviceDetails=new DeviceDetails();
			deviceDetails.setLoginID(login_id);
			deviceDetails.setCreatedTime(new Date());
			deviceDetails.setDeviceID(loginBO.getDeviceID());
			deviceDetails.setDeviceName(loginBO.getDeviceName());
			deviceDetails.setDevicePushID(loginBO.getDevicePushID());
			deviceDetails.setDeviceType(loginBO.getDeviceType());
			deviceDetails.setStatusCode(StatusCode.ENABLE);

		}
		return this.deviceDetailRepository.save(deviceDetails);
	}

}
