package com.dropu2.oauth.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.dropu2.oauth.entity.UserDetail;
import com.dropu2.oauth.repository.UserDetailRepository;
import com.dropu2.oauth.service.UserDetailService;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@Service
public class UserDetailServiceImpl implements UserDetailService {

	@javax.annotation.Resource
	private UserDetailRepository userDetailRepository;

	@Override
	public UserDetail findUserbyLoginID(Long ID) {
		return this.userDetailRepository.findByLoginId(ID);
	}

	@Override
	public UserDetail saveUserDetail(UserDetail userDetail) {
		UserDetail detail = findUserbyLoginID(userDetail.getLoginId());
		if (detail == null && (userDetail.getID() == null || userDetail.getID() == 0)) {
			userDetail.setCreatedTime(new Date());
		} else {
			userDetail.setModifyTime(new Date());
			if (userDetail.getID() == null) {
				userDetail.setID(detail.getID());
			}
		}
		return userDetailRepository.save(userDetail);
	}

	@Override
	public List<UserDetail> fetchAllUser() {
		return (List<UserDetail>) userDetailRepository.findAll();
	}

	@Override
	public Optional<UserDetail> findUserbyUserID(Long ID) {
		return userDetailRepository.findById(ID);
	}

}
