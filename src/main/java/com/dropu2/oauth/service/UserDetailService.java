package com.dropu2.oauth.service;

import java.util.List;
import java.util.Optional;

import com.dropu2.oauth.entity.UserDetail;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface UserDetailService {
	UserDetail findUserbyLoginID(Long ID);

	UserDetail saveUserDetail(UserDetail userDetail);

	Optional<UserDetail> findUserbyUserID(Long ID);

	List<UserDetail> fetchAllUser();
}
