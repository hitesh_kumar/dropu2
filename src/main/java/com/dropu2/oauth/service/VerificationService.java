package com.dropu2.oauth.service;

import com.dropu2.enume.VerificationType;
import com.dropu2.oauth.entity.Verification;

/**
 * @author NAROTTAMS
 * @created_date Sep 20, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
public interface VerificationService {
	Verification generateVerificationCode(VerificationType verification_type, String email_id, Long id);

	Verification validateVerificationCode(VerificationType verification_type,  Long id,
			String verifyCode);

	boolean sentEmail(String email_id, Verification verification);
	
	Verification save(Verification verification);
}

