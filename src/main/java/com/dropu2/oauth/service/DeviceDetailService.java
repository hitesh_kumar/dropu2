package com.dropu2.oauth.service;

import com.dropu2.enume.StatusCode;
import com.dropu2.model.LoginBO;
import com.dropu2.oauth.entity.DeviceDetails;

/**
 * @author NAROTTAMS
 * @created_date Sep 26, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
public interface DeviceDetailService {

	DeviceDetails findDeviceByLoginIDStatusCode(Long login_id, StatusCode statusCode);

	DeviceDetails saveDevice(LoginBO loginBO, Long login_id);

}
