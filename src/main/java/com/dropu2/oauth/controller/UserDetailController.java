package com.dropu2.oauth.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.oauth.entity.UserDetail;
import com.dropu2.oauth.service.UserDetailService;

@RestController
@RequestMapping("/user")
public class UserDetailController {

	@javax.annotation.Resource
	private UserDetailService uDetailService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public UserDetail saveUser(@RequestBody UserDetail userDetail) {
		return uDetailService.saveUserDetail(userDetail);
	}

	@RequestMapping(value = "/fetch/byuserid/{id}", method = RequestMethod.GET)
	public Optional<UserDetail> getUser(@PathVariable("id") Long id) {
		return uDetailService.findUserbyUserID(id);
	}

	@RequestMapping(value = "/fetch/{login_id}", method = RequestMethod.GET)
	public UserDetail getUserByLogin(@PathVariable("login_id") Long login_id) {
		return uDetailService.findUserbyLoginID(login_id);
	}

	@RequestMapping(value = "/fetch/all", method = RequestMethod.GET)
	public List<UserDetail> fetchALlUsers() {
		return uDetailService.fetchAllUser();
	}

}
