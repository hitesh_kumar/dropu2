package com.dropu2.oauth.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dropu2.oauth.entity.ImageData;
import com.dropu2.oauth.service.ImageDataService;

/**
 * @author NAROTTAMS
 * @created_date Sep 24, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@RestController
@RequestMapping("/image")
public class ImageController {

	@javax.annotation.Resource
	private ImageDataService iDataService;

	@RequestMapping(value = "/{refID}/upload", method = RequestMethod.POST)
	public ImageData uplaodImage(@RequestParam("imageFile") MultipartFile file, @RequestParam("refID") Long refID)
			throws IOException {
		return iDataService.saveImage(file, refID);
	}

	@GetMapping(path = { "/get/{refID}" })
	public ImageData getImageByRefID(@PathVariable("refID") Long refID) throws IOException {
		return iDataService.findByRefID(refID);
	}

}