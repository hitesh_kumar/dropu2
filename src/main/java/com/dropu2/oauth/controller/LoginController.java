package com.dropu2.oauth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dropu2.enume.StatusCode;
import com.dropu2.enume.VerificationType;
import com.dropu2.model.CustomResponse;
import com.dropu2.model.LoginBO;
import com.dropu2.oauth.entity.Login;
import com.dropu2.oauth.entity.Verification;
import com.dropu2.oauth.service.LoginService;
import com.dropu2.oauth.service.VerificationService;

/**
 * @author NAROTTAMS
 * @created_date Sep 20, 2020
 * @modify_by NAROTTAMS
 * @modify_time
 */
@RestController
@RequestMapping("/login")
public class LoginController {

	private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	@javax.annotation.Resource
	private LoginService loginService;

	@javax.annotation.Resource
	private VerificationService verificationService;

	@RequestMapping(value = "/loginuser", method = RequestMethod.POST)
	public ResponseEntity<CustomResponse<Login>> login(@RequestBody LoginBO login) {
		log.info("user enter in login()..");
		Login loginDB = loginService.login(login);

		if (loginDB != null) {
			ResponseEntity<Object> result = loginService.generateAuthToken(login.getEmailId(), login.getPassword());
			if (result != null && result.getStatusCode() == HttpStatus.OK) {
				CustomResponse<Login> customResponse = new CustomResponse<Login>(loginDB);
				customResponse.setMessage("Login Successfully");
				customResponse.setStatus("SUCCESS");
				customResponse.setToken(result.getBody());
				loginDB.setPassword(null);
				customResponse.setData(loginDB);
				return new ResponseEntity<>(customResponse, HttpStatus.OK);
			} else {
				CustomResponse<Login> customResponse = new CustomResponse<Login>(loginDB);
				customResponse.setMessage("Authentication failed");
				customResponse.setStatus("UNAUTHORIZED");
				customResponse.setData(null);
				return new ResponseEntity<>(customResponse, HttpStatus.UNAUTHORIZED);

			}
		} else {
			CustomResponse<Login> customResponse = new CustomResponse<Login>(loginDB);
			customResponse.setMessage("Detail does not match");
			customResponse.setStatus("FAIL");
			return new ResponseEntity<>(customResponse, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseEntity<CustomResponse<Login>> signup(@RequestBody LoginBO login) {
		log.info("user enter in signup()..");
		Login loginDB = loginService.signup(login);

		if (loginDB != null && loginDB.getLoginStatus() != StatusCode.ALREADY_EXIST) {
			ResponseEntity<Object> result = loginService.generateAuthToken(login.getEmailId(), login.getPassword());
			if (result != null && result.getStatusCode() == HttpStatus.OK) {

				CustomResponse<Login> customResponse = new CustomResponse<Login>(loginDB);
				loginDB.setPassword(null);
				customResponse.setData(loginDB);
				customResponse.setMessage("User created successfully");
				customResponse.setStatus("CREATED");
				customResponse.setToken(result.getBody());
				return new ResponseEntity<>(customResponse, HttpStatus.CREATED);
			} else {
				CustomResponse<Login> customResponse = new CustomResponse<Login>(loginDB);
				customResponse.setMessage("Authentication failed");
				customResponse.setStatus("UNAUTHORIZED");
				return new ResponseEntity<>(customResponse, HttpStatus.UNAUTHORIZED);

			}
		} else {
			CustomResponse<Login> customResponse = new CustomResponse<Login>(loginDB);
			customResponse.setMessage("Provided details al-ready exist");
			customResponse.setStatus("ALREADY");
			customResponse.setData(null);
			return new ResponseEntity<>(customResponse, HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(value = "/refreshtoken", method = RequestMethod.POST)
	public ResponseEntity<CustomResponse<Login>> refreshOauthToken(
			@RequestParam("refresh_token") String refresh_token) {
		ResponseEntity<Object> result = loginService.refreshAuthToken(refresh_token);
		if (result != null && result.getStatusCode() == HttpStatus.OK) {
			CustomResponse<Login> customResponse = new CustomResponse<Login>(null);
			customResponse.setMessage("Oauth token successfully refreshed");
			customResponse.setStatus("SUCCESS");
			customResponse.setToken(result.getBody());
			return new ResponseEntity<>(customResponse, HttpStatus.OK);
		} else {
			CustomResponse<Login> customResponse = new CustomResponse<Login>(null);
			customResponse.setMessage("Authentication failed");
			customResponse.setStatus("UNAUTHORIZED");
			customResponse.setToken(result.getBody());
			return new ResponseEntity<>(customResponse, HttpStatus.UNAUTHORIZED);

		}
	}

	@RequestMapping(value = "/verification/{verification_type}/{email_id}/{id}", method = RequestMethod.GET)
	public ResponseEntity<CustomResponse<Verification>> sendVerificationLink(
			@PathVariable("verification_type") VerificationType verification_type,
			@PathVariable("email_id") String email_id, @PathVariable("id") Long id) {
		Verification vF = verificationService.generateVerificationCode(verification_type, email_id, id);
		CustomResponse<Verification> customResponse = new CustomResponse<Verification>(vF);
		if (vF != null) {
			boolean flag = verificationService.sentEmail(email_id, vF);
			if (flag) {
				customResponse.setMessage(
						"Verification code has been sent your email id. Please login and enter 6 digit code!");
				customResponse.setStatus("EMAILSENT");
				customResponse.setData(null);
				return new ResponseEntity<>(customResponse, HttpStatus.OK);
			} else {
				customResponse.setMessage("Not able to sent email. Please try again!");
				customResponse.setStatus("FAIL");
				customResponse.setData(null);
				return new ResponseEntity<>(customResponse, HttpStatus.OK);
			}
		} else {
			customResponse.setMessage("Facing some technical issue. Please try again after some time!");
			customResponse.setStatus("FAIL");
			customResponse.setData(null);
			return new ResponseEntity<>(customResponse, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/verify/{verification_type}/{id}/{verify_code}", method = RequestMethod.GET)
	public ResponseEntity<CustomResponse<Verification>> verifyCode(
			@PathVariable("verification_type") VerificationType verification_type, @PathVariable("id") Long id,
			@PathVariable("verify_code") String verify_code) {
		Verification vF = verificationService.validateVerificationCode(verification_type, id, verify_code);
		if (vF != null && vF.getvStatusCode() == StatusCode.VERIFICATION_PENDING) {
			int flag = loginService.updateEmailStatus(id, StatusCode.VERIFIED);
			CustomResponse<Verification> customResponse = new CustomResponse<Verification>(vF);
			if (flag != 0) {
				vF.setvStatusCode(StatusCode.VERIFIED);
				verificationService.save(vF);
				customResponse.setMessage("Email successfully verified. Thanku!");
				customResponse.setStatus("VERIFIED");
			} else {
				customResponse.setMessage("Email verification failed.");
				customResponse.setStatus("FAIL");

			}
			customResponse.setData(null);
			return new ResponseEntity<>(customResponse, HttpStatus.OK);
		}
		if (vF != null && vF.getvStatusCode() == StatusCode.VERIFIED) {
			CustomResponse<Verification> customResponse = new CustomResponse<Verification>(vF);
			customResponse.setMessage("Al-ready verified");
			customResponse.setStatus("SUCCESS");
			customResponse.setData(null);
			return new ResponseEntity<>(customResponse, HttpStatus.OK);
		} else {
			CustomResponse<Verification> customResponse = new CustomResponse<Verification>(vF);
			customResponse.setMessage("Facing some technical issue. Please try again after some time!");
			customResponse.setStatus("FAIL");
			customResponse.setData(null);
			return new ResponseEntity<>(customResponse, HttpStatus.OK);
		}

	}

}
