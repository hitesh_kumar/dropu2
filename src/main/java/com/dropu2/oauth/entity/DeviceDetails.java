package com.dropu2.oauth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.DeviceType;
import com.dropu2.enume.StatusCode;

/**
 * @author NAROTTAMS
 * @created_date Sep 26, 2020
 * @modify_by NAROTTAMS
 * @modify_time 
 */
@Entity(name = "device_detail")
public class DeviceDetails extends CreateUpdateDate{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3001526263492090744L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "device_id")
	private String deviceID;
	
	@Column(name = "device_name")
	private String deviceName;
	
	@Column(name = "device_push_id")
	private String devicePushID;
	
	@Column(name = "login_id")
	private Long loginID;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "device_type")
	private DeviceType deviceType;
	
	@Column(name = "device_status")
	@Enumerated(EnumType.STRING)
	private StatusCode statusCode;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDevicePushID() {
		return devicePushID;
	}

	public void setDevicePushID(String devicePushID) {
		this.devicePushID = devicePushID;
	}

	

	public Long getLoginID() {
		return loginID;
	}

	public void setLoginID(Long loginID) {
		this.loginID = loginID;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}
	
	

}
