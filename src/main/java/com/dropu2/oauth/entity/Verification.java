package com.dropu2.oauth.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.StatusCode;
import com.dropu2.enume.VerificationType;

@Entity(name = "verification")
public class Verification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5446811928180429286L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "ref_id")
	private Long refID;
	
	
	@Enumerated(EnumType.STRING)
	@Column(name = "verification_type")
	private VerificationType verificationType;
	
	@Column(name = "verification_code")
	private String verificationCode;
	
	@Column(name = "verification_link")
	private String verificationLink;
	
	@Column(name = "created_time")
	private Date createdTime;
	
	@Column(name = "expire_in_hours")
	private Integer expireInHours;
	
	@Column(name = "verification_status")
	@Enumerated(EnumType.STRING)
	private StatusCode vStatusCode;
	

	public Long getRefID() {
		return refID;
	}

	public void setRefID(Long refID) {
		this.refID = refID;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public VerificationType getVerificationType() {
		return verificationType;
	}

	public void setVerificationType(VerificationType verificationType) {
		this.verificationType = verificationType;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public String getVerificationLink() {
		return verificationLink;
	}

	public void setVerificationLink(String verificationLink) {
		this.verificationLink = verificationLink;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Integer getExpireInHours() {
		return expireInHours;
	}

	public void setExpireInHours(Integer expireInHours) {
		this.expireInHours = expireInHours;
	}

	public StatusCode getvStatusCode() {
		return vStatusCode;
	}

	public void setvStatusCode(StatusCode vStatusCode) {
		this.vStatusCode = vStatusCode;
	}
	
	
}
