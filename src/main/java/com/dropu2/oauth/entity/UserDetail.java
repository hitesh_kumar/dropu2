package com.dropu2.oauth.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dropu2.enume.GenderEnum;
import com.dropu2.enume.PaymentOption;
import com.dropu2.enume.RideType;

@Entity(name = "user_detail")
public class UserDetail extends CreateUpdateDate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;

	@Column(name = "gender")
	@Enumerated(EnumType.STRING)
	private GenderEnum genderEnum;

	@Column(name = "login_id")
	private Long loginId;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "contact_number")
	private String contactNumber;

	@Column(name = "contact_email")
	private String contactEmail;

	@Column(name = "dl_number")
	private String dlNumber;

	@Column(name = "dl_valid_till")
	private Date dlValidTill;

	@Column(name="ride_type")
	@Enumerated(EnumType.STRING)
	private RideType rideType;

	@Column(name="payment_option")
	@Enumerated(EnumType.STRING)
	private PaymentOption paymentOption;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public GenderEnum getGenderEnum() {
		return genderEnum;
	}

	public void setGenderEnum(GenderEnum genderEnum) {
		this.genderEnum = genderEnum;
	}

	

	public Long getLoginId() {
		return loginId;
	}

	public void setLoginId(Long loginId) {
		this.loginId = loginId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getDlNumber() {
		return dlNumber;
	}

	public void setDlNumber(String dlNumber) {
		this.dlNumber = dlNumber;
	}

	public Date getDlValidTill() {
		return dlValidTill;
	}

	public void setDlValidTill(Date dlValidTill) {
		this.dlValidTill = dlValidTill;
	}

	public RideType getRideType() {
		return rideType;
	}

	public void setRideType(RideType rideType) {
		this.rideType = rideType;
	}

	public PaymentOption getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(PaymentOption paymentOption) {
		this.paymentOption = paymentOption;
	}
	
	
}
